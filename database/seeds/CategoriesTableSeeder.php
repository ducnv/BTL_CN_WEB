<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        $src = [
            'manchester-united' => 'Man United',
            'chelsea' => 'Chelsea',
            'arsenal' => 'Arsenal',
            'liverpool' => 'Liverpool',
            'manchester-city' => 'Manchester City',
            'real-madrid' => 'Real Madrid',
            'barcelona' => 'Barcelona',
            'atletico-madrid' => 'Atletico Madrid',
            'sevilla' => 'Sevilla',
            'bayern-munich' => 'Bayern Munich',
            'borussia-dortmund' => 'Dortmund',
            'juventus' => 'Juventus',
            'inter-milan' => 'Inter Milan',
            'ac-milan' => 'AC Milan',
            'as-roma' => 'AS Roma',
            'napoli' => 'Napoli',
            'uefa-champion-league' => 'Champion League',
            'europa-league' => 'Europa League',
            'lich-thi-dau-ngoai-hang-anh' => 'Anh',
            'lich-thi-dau-la-liga-tay-ban-nha' => 'Tây Ban Nha',
            'lich-thi-dau-serie-italia' => 'Italia',
            'lich-thi-dau-bundesliga-duc' => 'Đức',
            'bang-xep-hang-ngoai-hang-anh' => 'Anh',
            'bang-xep-hang-la-liga-tay-ban-nha' => 'Tây Ban Nha',
            'bang-xep-hang-serie-a-bong-da-y' => 'Italia',
            'bang-xep-hang-bundesliga-duc' => 'Đức',
            'bang-xep-hang-ligue-1-hang-nhat-phap' => 'Pháp',
            'bang-xep-hang-eredivisie-bong-da-ha-lan' => 'Hà Lan',
            'link-sopcast' => 'Link SopCast'
        ];

        foreach ($src as $key => $value) {
            DB::table('categories')->insert([
                'name' => $value,
                'name_slug' => $key,
                'description' => 'Description',
                'updated_at' => new \MongoDate(time()),
                'created_at' => new \MongoDate(time())
            ]);
        }
    }
}

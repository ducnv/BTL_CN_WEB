<?php

use App\Events\Inst;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        // Create admin account
        DB::table('users')->insert([
            'usertype' => 'Admin',
            'username' => 'admin',
            'username_slug' => 'admin',
            'icon' => null,
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'remember_token' => str_random(10),
            'created_at' => new \MongoDate(time()),
            'updated_at' => new \MongoDate(time())
        ]);

        DB::table('settings')->insert([
            'key' => 'p-appnews',
            'value' => 'on'
        ]);
        DB::table('settings')->insert([
            'key' => 'p-applists',
            'value' => 'on'
        ]);
        DB::table('settings')->insert([
            'key' => 'p-appvideos',
            'value' => 'on'
        ]);
        DB::table('settings')->insert([
            'key' => 'siteposturl',
            'value' => 1
        ]);
        DB::table('settings')->insert([
            'key' => 'AutoInHomepage',
            'value' => true
        ]);
        DB::table('settings')->insert([
            'key' => 'languagetype',
            'value' => 'en_US'
        ]);
        DB::table('settings')->insert([
            'key' => 'sitefontfamily',
            'value' => 'Lato, Helvetica, Arial, sans-serif'
        ]);
        DB::table('settings')->insert([
            'key' => 'googlefont',
            'value' => 'Lato:400,500,500italic,600,700&amp;subset=latin,latin-ext'
        ]);

        // factory('App\Models\User', 20)->create();
    }
}

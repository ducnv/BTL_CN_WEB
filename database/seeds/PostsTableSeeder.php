<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('posts')->truncate();

        for ($i = 0; $i < 9; $i++) {
            DB::table('products')->insert([
                'name' => 'Quần bò - QB'.($i+1),
                'name_slug' => 'quan-bo-QB'.($i+1),
                'category_id' => '571651cd6323882e188b45a4',
                'price' => random_int('300', '700'),
                'size' => ['M', 'L', 'XL', 'XXL'],
                'status' => 'yes',
                'thumb' => 'assets/img/quan-bo/'.($i+1).'.jpg',
                'remember_token' => str_random(10),
                'created_at' => new \MongoDate(time()),
                'updated_at' => new \MongoDate(time())
            ]);

        }
    }
}

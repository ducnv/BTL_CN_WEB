<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 28/01/2016
 * Time: 17:34
 */

namespace App\Repository\Posts;

use App\Models\Posts;
use Cache;

class EloquentPostRepository implements PostRepository
{
    public function lastFeatures($HomeColSec1Type1)
    {
//        $last_features = Cache::remember('lastFeatures', env('CACHE_TIME', 5), function () use ($HomeColSec1Type1) {
        return Posts::forhome()->typesAccepted($HomeColSec1Type1)->typesActivete()->approve('yes')->latest("published_at")->paginate(23);
//        });
        return $last_features;
    }

    public function lastNews($HomeColSec2Type1)
    {
        return Posts::forhome()->typesAccepted($HomeColSec2Type1)->typesActivete()->approve('yes')->latest("published_at")->take(15)->get();
    }

    public function lastTrendingVideos($HomeColSec3Type1)
    {
        return Posts::forhome()->typesAccepted($HomeColSec3Type1)->typesActivete()->approve('yes')->latest("published_at")->take(10)->get();
        return $last_trending_video;
    }

    public function lastFeaturesTop()
    {
        return Posts::forhome('Features')->typesActivete()->approve('yes')->whereNotNull('featured_at')->latest("featured_at")->take(4)->get();

        return $last_features_top;
    }

    public function lastVideosCol1()
    {
        return Posts::forhome()->byType('video')->typesActivete()->approve('yes')->latest("published_at")->get();
        return $last_videos_col1;
    }

    public function lastPoll()
    {
        return Posts::forhome()->byType('poll')->typesActivete()->approve('yes')->latest("published_at")->take(2)->get();
        return $last_poll;
    }

    public function lastTrending()
    {
        return Posts::forhome()->typesActivete()->approve('yes')->getStats('one_day_stats', 'DESC', 10)->get();
        return $last_trending;
    }
}
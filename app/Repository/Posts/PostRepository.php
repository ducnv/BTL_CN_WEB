<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 28/01/2016
 * Time: 17:34
 */

namespace App\Repository\Posts;

interface PostRepository
{
    public function lastFeatures($HomeColSec1Type1);

    public function lastNews($HomeColSec2Type1);

    public function lastTrendingVideos($HomeColSec3Type1);

    public function lastFeaturesTop();

    public function lastVideosCol1();

    public function lastPoll();

    public function lastTrending();
}
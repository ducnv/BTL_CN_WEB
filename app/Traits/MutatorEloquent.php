<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 06/01/2016
 * Time: 16:07
 */

namespace App\Traits;


trait MutatorEloquent
{
    public function getRawDataAttribute($value)
    {
        return json_decode($value);
    }

    public function setRawDataAttribute($value)
    {
        $this->attributes['raw_data'] = json_encode($value);
    }
}
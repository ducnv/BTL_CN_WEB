<?php
/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 29/01/2016
 * Time: 13:14
 */

namespace App\Traits;

use Image;

trait BaseCommand
{
    public function resizepostimage($imgWW, $slug)
    {
        $tmpFilePath = public_path('upload/media/posts/');

        $tmpFileDate = date('Y-m') . '/' . date('d') . '/';

        $tmpFileName = substr($slug, 0, 100) . '_' . time();

        $saveFilePath = $tmpFilePath . $tmpFileDate . $tmpFileName;

        $this->makeimagedir($tmpFilePath . $tmpFileDate);

        if (substr($imgWW, 0, 4) == 'http') {
            $imgWsr = $imgWW;
        } else {
            $imgWsr = substr($imgWW, 1);
        }

        $imgWW = Image::make($imgWsr);
        $imgWW2 = Image::make($imgWsr);

        $imbig = $imgWW->fit(650, 370)->save($saveFilePath . '-b.jpg');
        $imsmal = $imgWW2->fit(300, 190)->save($saveFilePath . '-s.jpg', 85);

        if (env('APP_FILESYSTEM') == "s3") {
            \Storage::disk('s3')->put($saveFilePath . '-b.jpg', $imbig->stream()->__toString());

            \Storage::disk('s3')->put($saveFilePath . '-s.jpg', $imsmal->stream()->__toString());

            \File::delete(public_path($saveFilePath . '-b.jpg'));
            \File::delete(public_path($saveFilePath . '-s.jpg'));

            return $this->s3url . $saveFilePath;
        }
        return $tmpFileDate . $tmpFileName;
    }

    public function makeimagedir($path)
    {
        if (!file_exists($path)) {
            $oldmask = umask(0);
            mkdir($path, 0777, true);
            umask($oldmask);
        }
        return;
    }

    public function saveMedia($url, $file_path)
    {
        try {
            if (file_put_contents($file_path, file_get_contents($url))) {
                if (filesize($file_path) > 0) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }
}
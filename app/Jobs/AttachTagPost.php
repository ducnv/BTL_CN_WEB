<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Posts;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AttachTagPost extends Job implements ShouldQueue, SelfHandling
{
    use SerializesModels, InteractsWithQueue;

    protected $post;

    protected $tags;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Posts $post, $tags = null)
    {
        $this->post = $post;
        $this->tags = $tags;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->tags) {
            $this->post->retag($this->tags);
        }
    }
}

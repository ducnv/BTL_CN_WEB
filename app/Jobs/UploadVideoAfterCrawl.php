<?php

namespace App\Jobs;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use App\Jobs\Job;
use App\Models\Entrys;
use App\Models\Posts;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadVideoAfterCrawl extends Job implements ShouldQueue, SelfHandling
{
    use SerializesModels, InteractsWithQueue;

    /**
     * @var Posts
     */
    protected $post;

    /**
     * @var string mp4_url
     */
    protected $file_path;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Posts $post, $file_path)
    {
        $this->post = $post;
        $this->file_path = $file_path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $title = $this->post['title'];
        $description = $this->post['description'];
        $file_path = $this->file_path;

        $page_id = env('FACEBOOK_PAGE_ID');
        $page_token = env('FACEBOOK_APP_TOKEN');
        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v2.5',
            'http_client_handler' => 'stream'
        ]);

        $data = [
            'title' => $title,
            'description' => $title,
            'source' => $fb->fileToUpload($file_path),
            'no_story' => $this->checkNeedPost($title),
            'content_category' => 'SPORTS'
        ];

        try {
            $response = $fb->post("/$page_id/videos", $data, $page_token);
        } catch (FacebookResponseException $e) {
            // TODO: Send email notification
            $fb_errors = $e->getMessage();
            \Mail::send('emails.remind', ['message' => $fb_errors], function ($m) use ($fb_errors) {
                $m->from('alert@vidgol.com', 'App App Error');
                $m->to('imtanlinh@gmail.com')->subject('App Notification About Facebook Crawl');
            });
            echo $fb_errors;
            exit;
        } catch (FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        $video_id = $graphNode['id'];
        echo 'VIDEO_ID = ' . $video_id . "\n";
        // publish post
        $this->post->approve = 'yes';
        $this->post->save();

        // TODO: Create entry
        $entry = new Entrys;
        $entry->user_id = 2;
        $entry->order = 0;
        $entry->type = 'video';
        $entry->title = '';
        $entry->video = "https://www.facebook.com/$page_id/videos/$video_id";

        $this->post->entry()->save($entry);
    }

    public function checkNeedPost($title)
    {
        $tmp = strtolower($title);
        $clubs = [
            'arsenal',
            'bayern',
            'manchester',
            'barcelona',
            'real',
            'juve',
            'milan',
            'liverpool',
            'tottenham',
            'chelsea',
            'dortmund',
            'wolfsburg',
            'napoli',
            'fiorentina',
            'paris',
            'psg',
            'inter',
            'atletico',
            'madrid',
            'roma',
            'lazio',
            'leicester',
            'everton'
        ];
        foreach ($clubs as $club) {
            if (strpos($tmp, $club) !== false) {
                return false;
            }
        }
        return true;
    }
}

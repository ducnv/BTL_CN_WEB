<?php

Route::get('/', 'IndexController@index');

Route::get('{catname}', 'PageController@showCategory');

Route::get('{catname}/{slug}', 'PostsController@index');

Route::post('gio-hang', 'PageController@cartProduct');

Route::controller('api', 'APIController');


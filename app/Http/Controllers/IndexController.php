<?php

namespace App\Http\Controllers;

use App\Events\Inst;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Models\Categories;
use App\Models\Products;

class IndexController extends Controller
{
    public $category_1 = "so-mi-dai-tay";
    public $category_2 = "ao-phong";
    public $category_3 = "quan-vai-gia-bo";
    public $category_4 = "quan-au";
    public $category_5 = "quan-ngo";
    public $category_6 = "ao-khoac-bo";
    public $category_7 = "that-lung";

    public function index()
    {
        $category_1 = Categories::where('name_slug', $this->category_1)->first();
        $name_cate_1 = $category_1->name;
        $name_slug_1 = $category_1->name_slug;
        $product_1 = Products::where('category_id', $category_1->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_2 = Categories::where('name_slug', $this->category_2)->first();
        $name_cate_2 = $category_2->name;
        $name_slug_2 = $category_2->name_slug;
        $product_2 = Products::where('category_id', $category_2->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_3 = Categories::where('name_slug', $this->category_3)->first();
        $name_cate_3 = $category_3->name;
        $name_slug_3 = $category_3->name_slug;
        $product_3 = Products::where('category_id', $category_3->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_4 = Categories::where('name_slug', $this->category_4)->first();
        $name_cate_4 = $category_4->name;
        $name_slug_4 = $category_4->name_slug;
        $product_4 = Products::where('category_id', $category_4->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_5 = Categories::where('name_slug', $this->category_5)->first();
        $name_cate_5 = $category_5->name;
        $name_slug_5 = $category_5->name_slug;
        $product_5 = Products::where('category_id', $category_5->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_6 = Categories::where('name_slug', $this->category_6)->first();
        $name_cate_6 = $category_6->name;
        $name_slug_6 = $category_6->name_slug;
        $product_6 = Products::where('category_id', $category_6->id)->orderby('created_at', 'DESC')->take(8)->get();

        $category_7 = Categories::where('name_slug', $this->category_7)->first();
        $name_cate_7 = $category_7->name;
        $name_slug_7 = $category_7->name_slug;
        $product_7 = Products::where('category_id', $category_7->id)->orderby('created_at', 'DESC')->take(8)->get();

       return view('page.index', compact('product_1','name_cate_1', 'name_slug_1', 'product_2','name_cate_2' , 'name_slug_2', 'product_3','name_cate_3', 'name_slug_3'
           , 'product_4','name_cate_4', 'name_slug_4', 'product_5','name_cate_5', 'name_slug_5', 'product_6','name_cate_6', 'name_slug_6', 'product_7','name_cate_7', 'name_slug_7'));
    }


}
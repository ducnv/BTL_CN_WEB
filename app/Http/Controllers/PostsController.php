<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Products;
use App\Http\Requests;

class PostsController extends Controller
{
    public function index($catname, $slug){
        $category = Categories::where('name_slug', $catname)->first();

        if (!$category) {
            abort('404');
        }

        if($catname == "lien-he"){

        }

        $name_cate = $category->name;
        $name_slug = $category->name_slug;

        $product = Products::where('category_id', $category->id)->where('name_slug', $slug)->first();
        if(count($product) != 0){
            return view ('page.infproduct', compact('name_cate', 'name_slug', 'product'));
        }
    }
}

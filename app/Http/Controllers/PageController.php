<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Products;
use App\Http\Requests;
use Session;

class PageController extends Controller
{

    public function showCategory($catname)
    {

        $category = Categories::where('name_slug', $catname)->first();

        if (!$category) {
            abort('404');
        }

        $name_cate = $category->name;
        $name_slug = $category->name_slug;

        if ($catname == "lien-he") {
            return view("page.lienhe", compact("name_cate", "name_slug"));
        }


        $product = Products::where('category_id', $category->id)->where('status', 'yes')->orderby('created_at', 'DESC')->take(8)->get();

        return view("page.showcategory", compact("product", "name_cate", "name_slug"));

    }

    public function cartProduct(Request $request)
    {

        $pro_id = $request->get('pro_id');
        $size = $request->get('size_pro');
        $quantity = $request->get('input_quantity');

        $inf_pro = Products::where('_id', $pro_id)->first();

        $inf = [
            'pro_id'=> $pro_id,
            'pro_name' => $inf_pro->name,
            'pro_slug' => $inf_pro->name_slug,
            'pro_thumb' => $inf_pro->thumb,
            'price' => $inf_pro->price,
            'size' => $size,
            'quantity' => $quantity
        ];

        $infs = Session::get('cart');
        var_dump(Session::get('cart'));
        if($infs == NULL){
            Session::put('cart',$inf);
        }else{
            array_push($infs,$inf);
            Session::put('cart',$infs);
        }
        Session::save();
        return view("page.cartproduct");
    }
}

<?php

/**
 * Created by PhpStorm.
 * User: tanlinh
 * Date: 16/03/2016
 * Time: 09:18
 */

return [

    'header' => [
        'thoi-trang-mua-he' => [
            'name' => 'Thời trang mùa hè',
            'type' => 'dropdown',
            'child' => [
                'so-mi-ngan-tay' => 'Sơ mi ngắn tay',
                'so-mi-dai-tay' => 'Sơ mi dài tay',
                'do-lot' => 'Đồ lót',
                'ao-phong' => 'Áo phông',
                'quan-vai-gia-bo' => 'Quần vải giả bò',
                'quan-au' => 'Quần âu',
                'quan-kaki' => 'Quần kaki',
                'quan-ngo' => 'Quần ngố'
            ]
        ],
        'thoi-trang-mua-dong' => [
            'name' => 'Thời trang mùa đông',
            'type' => 'dropdown',
            'child' => [
                'quan-bo' => 'Quần bò',
                'ao-khoac-ni' => 'Áo khoác nỉ',
                'ao-gio' => 'Áo gió',
                'ao-phao' => 'Áo phao',
                'ao-gile' => 'Áo gile',
                'ao-mang-to' => 'Áo măng tô',
                'vest' => 'Vest',
                'ao-khoac-bo' => 'Áo khoác bò',
                'ao-da-1' => 'Áo dạ',
                'ao-da-2' => 'Áo da',
                'ao-len' => 'Áo len',
                'ao-khoac-nam' => 'Áo khoác nam',
                'bo-the-thao' => 'Bộ thể thao',
                'ao-thun' => 'Áo thun',
                'ao-kaki' => 'Áo kaki',
                'quan-ni' => 'Quần nỉ'
            ]
        ],
        'phu-kien' => [
            'name' => 'Phụ kiện',
            'type' => 'dropdown',
            'child' => [
                'that-lung' => 'Thắt lưng',
                'tui-xach' => 'Túi xách',
                'vi-da' => 'Ví da',
                'vong-tay' => 'Vòng tay',
                'khang-quang-gang-tay' => 'Khăng quàng, gang tay',
                'dong-ho' => 'Đồng hồ',
                'kinh-mat' => 'Kính mắt'
            ]
        ],
        'giay-da-nam' => [
            'name' => 'Giày da nam',
            'type' => 'item'
        ],
        'tin-tuc' => [
            'name' => 'Tin tức',
            'type' => 'item'
        ],
        'lien-he' => [
            'name' => 'Liên hệ',
            'type' => 'item'
        ]


    ]
];
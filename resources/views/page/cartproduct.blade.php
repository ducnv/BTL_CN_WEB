@extends("app")

@section("header")
    <link rel="stylesheet" href="/assets/css/showcategory.css">
@endsection

@section("content")

    <div class="container">
        <div class="location white">
            <a href="/">Trang chủ</a> &gt;&gt;
            {{--<a href="{{url($name_slug)}}" class="current">{{$name_cate}}</a>--}}
        </div>

        <hr>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên sản phẩm</th>
                <th>Giá sản phẩm</th>
                <th>Số lượng</th>
                <th>Tổng</th>
                <th>Xóa</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $infs = Session::get('cart');
            var_dump($infs);die;
            ?>
            @foreach($infs as$key=> $inf)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>
                        <img src=" {{$inf['pro_thumb']}}"
                             style="vertical-align: middle; margin-right: 10px; float:left; width:60px;">


                        <a href=""><b> {{$inf['pro_name']}}</b></a>
                        <br>
                        28 | Màu 1
                    </td>
                    <td>
                        <span id="sell_price_pro_1098"> {{$inf['price']}}.000 VND</span>
                    </td>
                    <td>
                        <input name="quantity_pro_1098" id="quantity_pro_1098" value="{{$inf['quantity']}}"
                               size="10">
                    </td>
                    <td>
                        <span id="price_pro_1098">450.000</span>
                    </td>
                    <td>
                        <button onclick="delete_pro(this)" data-delete={{$key}} class="btn-danger">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4">
                    <div class="support_in_cart">
                        Bạn gặp khó khăn trong việc đặt hàng? Vui lòng nhấc máy để được trợ giúp: &nbsp;&nbsp;<b
                                class="font18 red">091 3906166</b>
                    </div>
                </td>
                <td colspan="2">
                    <b>Tổng tiền:</b>
                    <b style="color:red;"><span class="sub1" id="total_value" style="color: red; font-weight: bold;">2.270.000</span>
                        VND</b>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <hr>
@endsection

<script>
    function delete_pro(target) {
        var key = $(target).data('key');

        $.ajax({
            method: "GET",
            url: '/api/delete-product/',
            data: {key: key},
            success: function (data) {
                resetData(data);
            },
            error: function (data) {
                alert('error!');
            }
        });

    }
</script>
@extends("app")

@section("header")
    <link rel="stylesheet" href="/assets/css/app.css">
@endsection

<div class="container">
    @include("_particles.sildebar")
</div>

@section("content")

    <div class="container">

        <p class="name_category">{{$name_cate_1}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_1 as $product)
                <div class="col-xs-6 col-md-3">
                    <a href="{{url($name_slug_1."/".$product->name_slug)}}" class="thumbnail">
                        <img data-src="holder.js/100%x180"
                             alt="100%x180"
                             src="{{$product->thumb}}"
                             style="height: 300px; width: 100%; display: block;">
                    </a>
                    <div class="bg_pro">
                        <a href="{{url($name_slug_1."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                        <div class="price">Giá bán: {{$product->price}}.000đ</div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_2}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_2 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_2."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_2."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_3}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_3 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_3."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_3."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_4}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_4 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_4."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_4."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_5}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_5 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_5."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_5."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_6}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_6 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_6."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_6."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <p class="name_category">{{$name_cate_7}}</p>
        <hr>
        <div class="bs-example" data-example-id="simple-thumbnails">
            <div class="row">
                @foreach($product_7 as $product)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{url($name_slug_7."/".$product->name_slug)}}" class="thumbnail">
                            <img data-src="holder.js/100%x180"
                                 alt="100%x180"
                                 src="{{$product->thumb}}"
                                 style="height: 300px; width: 100%; display: block;">
                        </a>
                        <div class="bg_pro">
                            <a href="{{url($name_slug_7."/".$product->name_slug)}}" class="name" title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                            <div class="price">{{$product->price}}.000đ</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>

@endsection

@extends("app")

@section("header")
    <link rel="stylesheet" href="/assets/css/showcategory.css">
@endsection

@section("content")

    <div class="container">
        <div class="location white">
            <a href="/">Trang chủ</a> &gt;&gt;
            <a href="{{url($name_slug)}}" class="current">{{$name_cate}}</a>
        </div>
    </div>

    <hr>
    <div class="bs-example" data-example-id="simple-thumbnails">
        <div class="row">
            @foreach($product as $product)
                <div class="col-xs-6 col-md-3">
                    <a href="{{url($name_slug."/".$product->name_slug)}}" class="thumbnail">
                        <img data-src="holder.js/100%x180"
                             alt="100%x180"
                             src="{{$product->thumb}}"
                             style="height: 300px; width: 100%; display: block;">
                    </a>
                    <div class="bg_pro">
                        <a href="{{url($name_slug."/".$product->name_slug)}}" class="name"
                           title="{{$product->name}} (hàng mới về)">{{$product->name}}</a>
                        <div class="price">Giá bán: {{$product->price}}.000đ</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
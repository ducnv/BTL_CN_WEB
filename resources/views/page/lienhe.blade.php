@extends("app")

@section("header")
    <link rel="stylesheet" href="/assets/css/showcategory.css">
@endsection

@section("content")

    <div class="container">
        <div class="location white">
            <a href="/">Trang chủ</a> &gt;&gt;
            <a href="{{url($name_slug)}}" class="current">{{$name_cate}}</a>
        </div>

        <hr>
        <div class="box_common">
            <div class="title_box_1"><h1>Liên hệ</h1></div><!--title_box_1-->
            <div class="clear"></div>
            <div style="width:45%; float:left;">
                <b>H2T-SHOP - CƠ SỞ I bán lẻ</b>
                <iframe width="450" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=213185943042949260157.0004f44e8b3821a09952f&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=20.997743,105.793276&amp;spn=0.020033,0.038538&amp;z=14&amp;output=embed"></iframe><br><small>View <a href="https://www.google.com/maps/ms?msa=0&amp;msid=213185943042949260157.0004f44e8b3821a09952f&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=20.997743,105.793276&amp;spn=0.020033,0.038538&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">H2T-SHOP - CƠ SỞ I bán lẻ</a> in a larger map</small>
                <div class="clear"></div>
                <div class="space2"></div>
                <b>H2T-SHOP - CƠ SỞ II bán buôn</b>
                <iframe width="450" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/ms?msa=0&amp;msid=213185943042949260157.0004f44e99a3f37c1aef7&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=21.009642,105.79879&amp;spn=0.002504,0.004828&amp;z=17&amp;output=embed"></iframe><br><small>View <a href="https://www.google.com/maps/ms?msa=0&amp;msid=213185943042949260157.0004f44e99a3f37c1aef7&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;ll=21.009642,105.79879&amp;spn=0.002504,0.004828&amp;z=17&amp;source=embed" style="color:#0000FF;text-align:left">H2T-SHOP - CƠ SỞ II bán</a> in a larger map</small>

            </div><!--float:left-->
            <div style="width:45%; float:right;">
                <p><strong>Chi nhánh - Hà Nội<br></strong></p>
                <p><strong>Cơ sở bán lẻ :</strong></p>
                <p><strong>CS 1 : 146 Xã Đàn - Đống Đa - Hà Nội &nbsp;</strong><strong>Hotline : 0913 906166</strong></p>
                <p><strong>CS2 : 83 Xuân Thủy, Cầu Giấy, Hà Nội Hotline :&nbsp;048 5877804</strong></p>
                <p><strong>CS 3: 106 Tô Hiệu, Hải Phòng Hotline:&nbsp;0941103838</strong></p>
                <p><strong>Cơ sở ONLINE:&nbsp;<strong>&nbsp;</strong></strong></p>
                <p><strong><strong><strong>Phòng 414 - Tòa Nhà Bắc Hà &nbsp;HH2 - Lê Văn Lương kéo dài - Thanh Xuân - Hà Nội</strong></strong><br>Hotline :&nbsp;0913906166</strong></p>
                <div class="clear"></div>


                <br>
                <style type="text/css">
                    .require { font-weight:bold; color:#F00}
                </style>

            </div><!--float:left-->
        </div>
    </div>




@endsection
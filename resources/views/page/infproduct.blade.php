@extends("app")

@section("header")
    <link rel="stylesheet" href="/assets/css/showcategory.css">
@endsection

@section("content")

    <div class="container">
        <div class="location white">
            <a href="/">Trang chủ</a> &gt;&gt;
            <a href="{{url($name_slug)}}" class="current">{{$name_cate}}</a>
        </div>
    </div>
    <hr>
    <div class="bs-example" data-example-id="simple-thumbnails">
        <div class="row">
            <div class="col-xs-6 col-md-5">
                <a href="#" class="thumbnail">
                    <img data-src="holder.js/100%x180"
                         alt="100%x180"
                         src="{{asset($product->thumb)}}"
                         style="height: 560px; width: 100%; display: block;">
                </a>
            </div>

            <?php
            $infs = Session::get('cart');
            var_dump($infs);
            ?>

            <div class="col-xs-6 col-md-6 col-lg-offset-1">
                <form action="{{url('gio-hang')}}" method="post" id="aaaa">
                    {{ csrf_field() }}
                    <input type="hidden" name="pro_id" value="{{$product->id}}">
                    <input type="hidden" name="size_pro">
                    <div id="overview">
                        <h1>{{$product->name}}</h1>
                        <!-- AddThis Button BEGIN -->
                        <div class="price_detail">
                            <table cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td>GIÁ BÁN:</td>
                                    <td>
                                        <b style="font-size:30px;">  {{$product->price}}.000đ</b><br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <table id="tbl_config" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td width="80">Kích thước:</td>
                                <td id="list_size">
                                    @foreach($product->size as $key => $size)
                                        <b id="{{$key}}" onclick="choosesize(this);" data-size="{{$size}}"
                                           class="size_{{$key}}">{{$size}}</b>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td width="80">Số lượng:</td>
                                <td><input type="text" style="width:50px; text-align:center; padding:0; height:20px;"
                                           value="1" name="input_quantity" id="input_quantity">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <p>&nbsp;</p>
                        <button type="submit" class="btn btn-info btn-lg">
                            <span class="glyphicon glyphicon-shopping-cart"></span>    Mua hàng
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

<script>
    function choosesize(target) {
        $("#list_size b").removeClass("selected");
        $(target).addClass("selected");
        var size = $(target).data('size');
        $('input[name="size_pro"]').val(size)
    }
    $("#aaaa").submit(function(e){
        e.preventDefault();
        return false;
    });
    function cartProduct(target) {
        target.preventDefault();
        alert('11');
        if (!($("#list_size b").hasClass("selected"))) {
            alert('chua chon size');
        }
    }

</script>
<hr>
<table>
    <tbody>
    <tr>
        <td>
            <h3>H2T-SHOP - CƠ SỞ bán lẻ</h3>
            CS 1: 146 Xã Đàn - Đống Đa - Hà Nội - Điện thoại: 0913 906166<br>
            CS 2 : 83 Xuân Thủy, Cầu Giấy, Hà Nội - Điện thoại : 048 5877804 <br>
            CS 3: 106 Tô Hiệu, Hải Phòng - Điện thoại: 0941103838<br>
        </td>
        <td>
            <h3>H2T-SHOP - Văn phòng ONLINE </h3>
            ĐC: P414 - Tòa nhà Bắc Hà - HH2 - Lê Văn Lương - (048) 5877804
        </td>
    </tr>
    </tbody>
</table>
<hr>
<div class="summary_bottom">
    Shop online H2T-SHOP đã khẳng định được sự chuyên nghiệp của mình trong công việc. Quần, áo tại H2T được cập nhật
    mới liên tục, mỗi mẫu đều có số lượng hàng đầy đủ để cung ứng cho khách hàng bán buôn và bán lẻ. Đội ngũ nhân viên
    của H2T trẻ trung, năng động và phong cách bán hàng chuyên nghiệp nhiệt tình...H2T-Shop cam kết bán hàng đúng giá
    niêm yết, chất lượng đảm bảo, bồi thường gấp đôi nếu có vấn để về chất lượng sản phẩm khi bán cho khách hàng.
    Shop online H2T luôn đem lại sự hài lòng đến với khách hàng!
</div>

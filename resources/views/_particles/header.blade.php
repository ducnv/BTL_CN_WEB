
<header class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Trang chủ</a>
        <div class="navbar-collapse nav-collapse collapse navbar-header">
            <ul class="nav navbar-nav">
                @foreach(config('menu.header') as $url => $item)
                    @if($item['type'] == 'dropdown')
                        <li class="dropdown create-links hor">
                            <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $item['name'] }}<span class="caret"></span></a>
                            <ul class="dropdown-menu sub-nav">
                                @foreach($item['child'] as $c_url => $child)
                                    <li>
                                        <a class="sub-item"
                                           href="{{ url($c_url) }}">{{ $child }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="create-links hor">
                            <a href="{{ url($url) }}">{{ $item['name'] }}</a>
                        </li>
                    @endif
                @endforeach

            </ul>
        </div> <!-- .nav-collapse -->
    </div> <!-- .container -->
</header>


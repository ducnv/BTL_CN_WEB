
<style>
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        width: 100%;
        margin: auto;
    }
</style>
<div id="myCarousel" class="container carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img style="width: 100%;" src="/assets/img/banner_9a115815.jpg" alt="Chania">
            <div class="carousel-caption">
                <h3></h3>
                <p></p>
            </div>
        </div>

        <div class="item">
            <img style="width: 100%;" src="/assets/img/banner_a684ecee.jpg" alt="Chania">
            <div class="carousel-caption">
                <h3></h3>
                <p></p>
            </div>
        </div>

        <div class="item">
            <img style="width: 100%;" src="/assets/img/banner_d9d4f495.jpg" alt="Chania">
            <div class="carousel-caption">
                <h3></h3>
                <p</p>
            </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
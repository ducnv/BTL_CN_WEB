<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">
<head>
    <title>@yield('head_title')</title>

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/bower_components/sweetalert/dist/sweetalert.css">

    @yield("header")

</head>
<body>
<div class="container">
    @include("_particles.header")


    <div class="content-wrapper" id="container-wrapper">

        @yield("content")

    </div>


    @include("_particles.footer")

    @yield("footer")
</div>
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script src="/bower_components/sweetalert/dist/sweetalert.min.js"></script>

</body>
</html>
